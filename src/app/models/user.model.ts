export class User {

    login: string;

    avatar_url: string;

    name?: string;

    followers_url?: string;

    created_at: Date;
}