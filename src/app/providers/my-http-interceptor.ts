import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { AlertService } from './services/alert.service';

/**
 * Global HTTP error handler (intercepts all http calls and handle client/server side errors in one place)
 */
export class MyHttpInterceptor implements HttpInterceptor {

    constructor(private alert: AlertService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request)
            .pipe(
                catchError((error: HttpErrorResponse) => {
                    let errorMessage: string = undefined;
                    if (error.error instanceof ErrorEvent) {
                        // error on Client side
                        errorMessage = error.error.message;
                    } else {
                        // error on Server side
                        errorMessage = `${error.message} (status: ${error.status})`;
                    }

                    //Maybe this isn't the best place to show alert (sometimes we don't want to show error messages to user).
                    //This is only example of intercepting all bad things from server and showing it to user.

                    //If you have need to store all errors from rest api into database maybe this is the right place.
                    //Here you can also manage loader component if you wan't to show loading animation for every http request

                    this.alert.showError(errorMessage);
                    console.log(errorMessage);
                    return throwError(errorMessage);
                })
            )
    }
}