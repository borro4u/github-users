import { Injectable } from '@angular/core';

import { environment } from '../../environments/environment';

/**
 * URL constants for REST API
 */
@Injectable({
    providedIn: 'root'
})
export class ApiUrls {

    //url for getting specific users (url example for fetching users with key 'test': 'search/users?q=test')
    public readonly GET_USERS = environment.serverUrl + 'search/users?q=';

    //url for fetching additional user information (url example for borro4u user: '/users/borro4u')
    public readonly GET_USER_ADD_INFO = environment.serverUrl + 'users/';
}