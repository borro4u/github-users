import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ApiUrls } from '../api-urls';

/**
 * Service for all actions on users
 */
@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private httpClient: HttpClient, private apiUrls: ApiUrls) { }

  getUsers(key: string): Observable<any> {
    return this.httpClient.get(this.apiUrls.GET_USERS + key);
  }

  getUserAdditionalInfo(userLogin: string): Observable<any> {
    return this.httpClient.get(this.apiUrls.GET_USER_ADD_INFO + userLogin);
  }

  getUserFollowers(userLogin: string): Observable<any> {
    return this.httpClient.get(this.apiUrls.GET_USER_ADD_INFO + userLogin + '/followers');
  }
}