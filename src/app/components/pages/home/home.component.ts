import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { DialogService } from 'ng6-bootstrap-modal';

import { AlertService } from '../../../providers/services/alert.service';
import { UserService } from '../../../providers/services/user.service';
import { UserModalComponent } from './user-modal/user-modal.component';
import { User } from '../../../models/user.model';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  users: Array<User>;
  //flag for loading animation when fetching users
  loading: boolean;

  constructor(
    private userService: UserService,
    private alertService: AlertService,
    private dialogService: DialogService) { }

  ngOnInit() {
    //default users
    this.getUsers('test');
  }

  onSubmit(f: NgForm) {
    if (!f.value || !f.value.key) {
      //if value is empty show default users
      this.getUsers('test');
    } else {
      this.getUsers(f.value.key);
    }
  }

  getUsers(key: string) {
    this.loading = true;

    this.userService.getUsers(key).subscribe(data => {

      this.loading = false;
      console.log('Users', data);

      if (data.items) {
        if (data.items.length > 10) {
          //we only need to show 10 users max
          this.users = data.items.slice(0, 10);
        } else {
          this.users = data.items;

          if (!this.users || !this.users.length) {
            //show warning message if there is no users
            this.alertService.showWarning(`There is no users with key ${key}!`);
          }
        }
      }
    },
      () => {
        this.loading = false;
      });
  }


  destroyUser() {
    this.alertService.showError('Haha, very funny!', 'Destroying user');
  }

  openAdditionalInfo(userLogin: string) {
    let disposable = this.dialogService.addDialog(UserModalComponent, {
      userLogin
    }).subscribe();
    //We can close dialog calling disposable.unsubscribe();
    //If dialog was not closed manually close it by timeout
    setTimeout(() => {
      disposable.unsubscribe();
    }, 10000);
  }
}