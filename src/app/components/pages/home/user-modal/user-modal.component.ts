import { Component, OnInit } from '@angular/core';
import { DialogComponent, DialogService } from "ng6-bootstrap-modal";

import { UserService } from '../../../../providers/services/user.service';
import { User } from '../../../../models/user.model';

export interface ConfirmModel { }

@Component({
  selector: 'app-user-modal',
  templateUrl: './user-modal.component.html',
  styleUrls: ['./user-modal.component.scss']
})
export class UserModalComponent extends DialogComponent<ConfirmModel, boolean> implements OnInit {

  userLogin: string;

  user: User;
  //user followers
  followers: Array<{ login: string, avatar_url: string }> = [];
  //pagination values for follower list
  pagination: { currentPage: number, itemsPerPage: number } = { currentPage: 1, itemsPerPage: 10 };

  constructor(
    dialogService: DialogService,
    private userService: UserService) {
    super(dialogService);
  }

  ngOnInit() {
    //user details (avatar, name, date joined)
    this.getUserAddInfo();
    //user followers
    this.getUserFollowers();
  }

  getUserAddInfo() {
    this.userService.getUserAdditionalInfo(this.userLogin).subscribe(data => {
      console.log('User details', data);
      this.user = data;
    });
  }

  getUserFollowers() {
    this.userService.getUserFollowers(this.userLogin).subscribe(data => {
      console.log('User followers', data);
      this.followers = data;
    });
  }

  confirm() {
    // we set dialog result as true on click on confirm button, 
    // then we can get dialog result from caller code 
    this.result = true;
    this.close();
  }
}