import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Ng6PaginationModule } from 'ng6-pagination';
import { BootstrapModalModule } from 'ng6-bootstrap-modal';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { UserModalComponent } from './user-modal/user-modal.component';
import { UserService } from '../../../providers/services/user.service';



@NgModule({
    declarations: [
        HomeComponent,
        UserModalComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        HomeRoutingModule,
        Ng6PaginationModule,
        BootstrapModalModule
    ],
    entryComponents: [
        UserModalComponent
    ],
    providers: [
        UserService
    ]
})

export class HomeModule { }